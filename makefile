make:
	php satis/bin/satis build rwth_composer_repo.json rwth_composer_repo

dependencies:
	composer create-project composer/satis:dev-master --keep-vcs

config:
	python3 config.py

install:
	cp -R rwth_composer_repo /var/www/html/

run:
	php -S localhost:8001 -t rwth_composer_repo/

clean:
	rm -Rf rwth_composer_repo
	rm rwth_composer_repo.json

clean_all:
	rm -Rf rwth_composer_repo
	rm -f rwth_composer_repo.json
	rm -Rf satis
	rm -f HOMEPAGE
