import json

try:
    file = open("rwth_composer_repo.json.template", "r")
    conf_raw = file.read()
    file.close()
except FileNotFoundError:
    print("ERROR: no rwth_composer_repo.json.template file")
    exit()
try:
    file = open("HOMEPAGE", "r")
    for line in file:
        homepage = line
        break
    file.close()
except FileNotFoundError:
    homepage = input("Enter your url: ")
    if not "http" in homepage:
        print("INFO: Maybe you forgot the protocol.")
    file = open("HOMEPAGE", "w")
    file.write(homepage)
    file.close()

conf = json.loads(conf_raw)

homepage = homepage.replace("\n", "")
homepage = homepage.replace("\r", "")
conf["homepage"] = homepage

conf_raw = json.dumps(conf)

file = open("rwth_composer_repo.json", "w")
file.write(conf_raw)
file.close()

