## Dependencies
To install dependencies, just execute `make dependencies`.

## Configure
Change the homepage url run `make config`

**"homepage" needs an protocol to work. Example https://repo.example.com**.

If you test it local you can insert `http://localhost:port`.

## Build
To build the Repository, execute `make`

## Install
Run `make install`. This will copy the folder rwth\_composer\_repo to /var/www/html. You need a webserver (Apache/NGINX) running in this folder.

## Run localy
If you have no webserver, you can run it with the php buildin webserver by running `make run`. This will start a webserver on http://localhost:8001

## Troubleshooting
### homepage : Invalid URL format
```
homepage : Invalid URL format

[Composer\Json\JsonValidationException]
The json config file does not match the expected JSON schema
```

Make sure you have the protocol in "homepage" like pointed out in ## Configure


